.global main
.func main
   
main:
    BL  _prompt             @ branch to prompt procedure and then scan in first number
    BL  _scanf 
    VMOV S0, R0             @ store first number
    
    BL  _getchar	    @ scan in operation
    MOV R11, R0		    @ store operation

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ add a comparison for only if the pow operation is activated

                            @ populate parameters
    MOV R3, R11

    BL  _eval               @ run the calculation

_prompt:
    MOV R7, #4              @ write syscall, 4
    MOV R0, #1              @ output stream to monitor, 1
    MOV R2, #148            @ print string length
    LDR R1, =prompt_str     @ string at label prompt_str:
    SWI 0                   @ execute syscall
    MOV PC, LR              @ return 

_printf:
    MOV R4, LR              @ store LR since printf call overwrites
    LDR R0, =printf_str     @ R0 contains formatted string address
    BL  printf              @ call printf
    B   main
    
_scanf:
    PUSH {LR}               @ store LR since scanf call overwrites
    SUB SP, SP, #4          @ make room on stack
    LDR R0, =format_str     @ R0 contains address of format string
    MOV R1, SP              @ move SP to R1 to store entry on stack
    BL  scanf               @ call scanf
    LDR R0, [SP]            @ load value at SP into R0
    ADD SP, SP, #4          @ restore the stack pointer
    POP {PC}                @ return

_getchar:
    MOV R7, #3              @ write syscall, 3
    MOV R0, #0              @ input stream from monitor, 0
    MOV R2, #1              @ read a single character
    LDR R1, =read_char      @ store the character in data memory
    SWI 0                   @ execute the system call
    LDR R0, [R1]            @ move the character to the return register
    AND R0, #0xFF           @ mask out all but the lowest 8 bits
    MOV PC, LR              @ return


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

@@@@@@

_eval:			    @ evaluates the char function
    CMP R3, #'a'            @ abs
    BEQ _abs

    CMP R3, #'s'            @ sqrt
    BEQ _sqrt

    CMP R3, #'p'            @ pow
    BEQ _pow

    CMP R3, #'i'            @ inv
    BEQ _inv

_abs:
    VABS.F32 S0, S0

    VCVT.F64.F32 D1, S0     @ covert the result to double precision for printing
    VMOV R1, R2, D1         @ split the double VFP register into two ARM registers

    BL  _printf

_sqrt:
    VSQRT.F32 S0, S0

    VCVT.F64.F32 D1, S0     @ covert the result to double precision for printing
    VMOV R1, R2, D1         @ split the double VFP register into two ARM registers

    BL  _printf

_pow:
    BL  _scanf 

    MOV S1, R0              @ populate parameters

    LOOP: CMP S1, #0.0
          VMULGT.F32 S0, S0, S0
	  VSUBGT.F32 S1, S1, #1.0
	  BGT LOOP


    VCVT.F64.F32 D1, S0     @ covert the result to double precision for printing
    VMOV R1, R2, D1         @ split the double VFP register into two ARM registers

    BL  _printf

_inv: 
    VLDR S1, =val1

    VCVT.F32.U32 S0, S0     @ convert unsigned bit representation to single float
    VCVT.F32.U32 S1, S1     @ convert unsigned bit representation to single float

    VDIV S2, S1, S0

    VCVT.F64.F32 D1, S2     @ covert the result to double precision for printing
    VMOV R1, R2, D1         @ split the double VFP register into two ARM registers

    BL  _printf 

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

@@@@@@

.data
val1:           .float      1.000
format_str:     .asciz      "%f"
read_char:      .ascii      " "
prompt_str:     .asciz      "Insert the first number, then the operation (a for abs, s for sqrt, p for pow, or i for inv), then if you entered p for pow, enter a second 

number: "
printf_str:     .asciz      "The result is: %f\n"
exit_str:       .ascii      "Terminating program.\n"